@echo off
set res=F
echo %1
echo %2
echo %3

IF "%~1" == "" GOTO SETUSER
IF "%~1" == "-ask" GOTO ASKUSER
IF NOT "%~1" == "" GOTO PARAMUSER

:SETUSER
echo SETUSER
set userName=#VUL IN#
set repName=#VUL IN#
GOTO DOSTUFF

:ASKUSER
echo ASKUSER
set /p userName=Username: 
set /p repName=Name of repository: 
GOTO DOSTUFF

:PARAMUSER
IF (%1=="") (GOTO SETUSER)
echo PARAMUSER
set userName=%1
set repName=%2

:DOSTUFF
echo UPLOAD TO DOCKER
echo %userName%/%repName%

docker build --tag %repName% .
docker tag %repName% %userName%/%repName%
docker push %userName%/%repName%
pause